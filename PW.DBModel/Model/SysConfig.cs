using System;
using System.Collections.Generic;

namespace PW.Common.ViewModel
{
    public class SysConfig
    {
                        private int _configId;
        
        /// <summary>
        /// 参数主键
        /// </summary>
        public int ConfigId
        {
            get
            {
				return _configId;
            }
            set
            {
				_configId = value;
            }
        }
                        private string _configName;
        
        /// <summary>
        /// 参数名称
        /// </summary>
        public string ConfigName
        {
            get
            {
				return _configName;
            }
            set
            {
				_configName = value;
            }
        }
                        private string _configKey;
        
        /// <summary>
        /// 参数键名
        /// </summary>
        public string ConfigKey
        {
            get
            {
				return _configKey;
            }
            set
            {
				_configKey = value;
            }
        }
                        private string _configValue;
        
        /// <summary>
        /// 参数键值
        /// </summary>
        public string ConfigValue
        {
            get
            {
				return _configValue;
            }
            set
            {
				_configValue = value;
            }
        }
                        private string _configType;
        
        /// <summary>
        /// 系统内置（Y是 N否）
        /// </summary>
        public string ConfigType
        {
            get
            {
				return _configType;
            }
            set
            {
				_configType = value;
            }
        }
                        private string _createBy;
        
        /// <summary>
        /// 创建者
        /// </summary>
        public string CreateBy
        {
            get
            {
				return _createBy;
            }
            set
            {
				_createBy = value;
            }
        }
                        private DateTime _createTime;
        
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime
        {
            get
            {
				return _createTime;
            }
            set
            {
				_createTime = value;
            }
        }
                        private string _updateBy;
        
        /// <summary>
        /// 更新者
        /// </summary>
        public string UpdateBy
        {
            get
            {
				return _updateBy;
            }
            set
            {
				_updateBy = value;
            }
        }
                        private DateTime _updateTime;
        
        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime UpdateTime
        {
            get
            {
				return _updateTime;
            }
            set
            {
				_updateTime = value;
            }
        }
                        private string _remark;
        
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark
        {
            get
            {
				return _remark;
            }
            set
            {
				_remark = value;
            }
        }
            }
}