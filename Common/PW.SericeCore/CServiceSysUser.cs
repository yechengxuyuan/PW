﻿using PW.Infrastructure;
using PW.ServiceCenter.ServiceSysUser;
using System;
using System.Collections.Generic;

namespace PW.ServiceCenter
{
    public class CServiceSysUser
    {
        #region 查询
        public event System.EventHandler<ServicesEventArgs<sys_user[]>> queryCompleted;
        public void query(sys_user record)
        {
            ServiceSysUserClient client = new ServiceSysUserClient();
            try
            {
                client.queryCompleted += (sender, e) =>
                {
                    ServicesEventArgs<sys_user[]> arg = new ServicesEventArgs<sys_user[]>();

                    if (e.Error == null)
                    {
                        arg.Result = e.Result;
                        arg.Succesed = true;
                    }
                    else
                    {
                        arg.Succesed = false;
                        arg.Error = e.Error;
                    }
                    if (queryCompleted != null)
                    {
                        queryCompleted.Invoke(this, arg);
                    }
                };
                client.queryAsync(record);
            }
            catch (Exception ex) {
                string s = ex.Message;
            }
            
        }
        #endregion

        #region 分页查询
        public event System.EventHandler<ServicesEventArgs<PageInfoOfsys_userCLUigIiY>> queryPageCompleted;
        public void queryPage(PageInfoOfsys_userCLUigIiY page)
        {
            ServiceSysUserClient client = new ServiceSysUserClient();
            try
            {
                client.queryPageCompleted += (sender, e) =>
                {
                    ServicesEventArgs<PageInfoOfsys_userCLUigIiY> arg = new ServicesEventArgs<PageInfoOfsys_userCLUigIiY>();

                    if (e.Error == null)
                    {
                        arg.Result = e.Result;
                        arg.Succesed = true;
                    }
                    else
                    {
                        arg.Succesed = false;
                        arg.Error = e.Error;
                    }
                    if (queryPageCompleted != null)
                    {
                        queryPageCompleted.Invoke(this, arg);
                    }
                };
                client.queryPageAsync(page);
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }
        }
        #endregion

        #region 删除
        public event System.EventHandler<ServicesEventArgs<int>> deleteByIdCompleted;
        public void deleteById(int id)
        {
            ServiceSysUserClient client = new ServiceSysUserClient();
            client.deleteByIdCompleted += (sender, e) =>
            {
                ServicesEventArgs<int> arg = new ServicesEventArgs<int>();

                if (e.Error == null)
                {
                    arg.Result = e.Result;
                    arg.Succesed = e.Result > 0;
                }
                else
                {
                    arg.Succesed = false;
                    arg.Error = e.Error;
                }
                if (deleteByIdCompleted != null)
                {
                    deleteByIdCompleted.Invoke(this, arg);
                }
            };
            client.deleteByIdAsync(id);
        }
        #endregion

        #region 添加
        public event System.EventHandler<ServicesEventArgs<int>> addCompleted;
        public void add(sys_user record)
        {
            ServiceSysUserClient client = new ServiceSysUserClient();
            client.addCompleted += (sender, e) =>
            {
                ServicesEventArgs<int> arg = new ServicesEventArgs<int>();

                if (e.Error == null)
                {
                    arg.Result = e.Result;
                    arg.Succesed = e.Result > 0;
                }
                else
                {
                    arg.Succesed = false;
                    arg.Error = e.Error;
                }
                if (addCompleted != null)
                {
                    addCompleted.Invoke(this, arg);
                }
            };
            client.addAsync(record);
        }
        #endregion
        #region 更新
        public event System.EventHandler<ServicesEventArgs<int>> updateByIdCompleted;
        public void updateById(sys_user record)
        {
            ServiceSysUserClient client = new ServiceSysUserClient();
            client.updateByIdCompleted += (sender, e) =>
            {
                ServicesEventArgs<int> arg = new ServicesEventArgs<int>();

                if (e.Error == null)
                {
                    arg.Result = e.Result;
                    arg.Succesed = e.Result > 0;
                }
                else
                {
                    arg.Succesed = false;
                    arg.Error = e.Error;
                }
                if (updateByIdCompleted != null)
                {
                    updateByIdCompleted.Invoke(this, arg);
                }
            };
            client.updateByIdAsync(record);
        }
        #endregion
        #region 添加
        public event System.EventHandler<ServicesEventArgs<sys_user>> getByIdCompleted;
        public void getById(int id)
        {
            ServiceSysUserClient client = new ServiceSysUserClient();
            client.getByIdCompleted += (sender, e) =>
            {
                ServicesEventArgs<sys_user> arg = new ServicesEventArgs<sys_user>();

                if (e.Error == null)
                {
                    arg.Result = e.Result;
                    arg.Succesed = true;
                }
                else
                {
                    arg.Succesed = false;
                    arg.Error = e.Error;
                }
                if (getByIdCompleted != null)
                {
                    getByIdCompleted.Invoke(this, arg);
                }
            };
            client.getByIdAsync(id);
        }
        #endregion
    }
}