﻿
  using PW.DBCommon.Model;
  using System.Collections.Generic;
  using System.ServiceModel;

  namespace PW.Service
  {
    [ServiceContract]
    public interface IServiceSysUser
    {
        /// <summary>
        /// 查询
        /// </summary>
        [OperationContract]
        List<sys_user> query(sys_user record);

        /// <summary>
        /// 分页查询
        /// </summary>
        [OperationContract]
        PageInfo<sys_user> queryPage(PageInfo<sys_user> page);

        /// <summary>
        /// 删除
        /// </summary>
        [OperationContract]
        int deleteById(int id);

        /// <summary>
        /// 添加
        /// </summary>
        [OperationContract]
        int add(sys_user record);

        /// <summary>
        /// 更新
        /// </summary>
        [OperationContract]
        int updateById(sys_user record);

        /// <summary>
        /// getById
        /// </summary>
        [OperationContract]
        sys_user getById(int id);
    }
  }
