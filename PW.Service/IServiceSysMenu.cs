using PW.DBCommon.Model;
  using System.Collections.Generic;
  using System.ServiceModel;

  namespace PW.Service
  {
    [ServiceContract]
    public interface IServiceSysMenu
    {
        /// <summary>
        /// 查询
        /// </summary>
        [OperationContract]
        List<sys_menu> query(sys_menu record);

        /// <summary>
        /// 分页查询
        /// </summary>
        [OperationContract]
        PageInfo<sys_menu> queryPage(PageInfo<sys_menu> record);

        /// <summary>
        /// 删除
        /// </summary>
        [OperationContract]
        int deleteById(long id);

        /// <summary>
        /// 添加
        /// </summary>
        [OperationContract]
        int add(sys_menu record);

        /// <summary>
        /// 更新
        /// </summary>
        [OperationContract]
        int updateById(sys_menu record);

        /// <summary>
        /// getById
        /// </summary>
        [OperationContract]
        sys_menu getById(long id);
    }
  }