using PW.DBCommon.Model;
  using System.Collections.Generic;
  using System.ServiceModel;

  namespace PW.Service
  {
    [ServiceContract]
    public interface IServiceSysConfig
    {
        /// <summary>
        /// 查询
        /// </summary>
        [OperationContract]
        List<sys_config> query(sys_config record);

        /// <summary>
        /// 分页查询
        /// </summary>
        [OperationContract]
        PageInfo<sys_config> queryPage(PageInfo<sys_config> record);

        /// <summary>
        /// 删除
        /// </summary>
        [OperationContract]
        int deleteById(int id);

        /// <summary>
        /// 添加
        /// </summary>
        [OperationContract]
        int add(sys_config record);

        /// <summary>
        /// 更新
        /// </summary>
        [OperationContract]
        int updateById(sys_config record);

        /// <summary>
        /// getById
        /// </summary>
        [OperationContract]
        sys_config getById(int id);
    }
  }