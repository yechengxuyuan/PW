﻿
  using PW.DBCommon.Dao;
  using PW.DBCommon.Model;
  using System.Collections.Generic;

  namespace PW.Service
  {
    public class ServiceSysUser : IServiceSysUser
    {
        /// <summary>
        /// 查询
        /// </summary>
        public List<sys_user> query(sys_user record)
        {
            return new SysUserDao().query(record);
        }
 
        /// <summary>
        /// 分页查询
        /// </summary>
        public PageInfo<sys_user> queryPage(PageInfo<sys_user> page)
        {
            return new SysUserDao().queryPage(page);
        }

        /// <summary>
        /// 删除
        /// </summary>
        public int deleteById(int id)
        {
            return new SysUserDao().deleteById(id);
        }

        /// <summary>
        /// 添加
        /// </summary>
        public int add(sys_user record)
        {
            return new SysUserDao().add(record);
        }

        /// <summary>
        /// 更新
        /// </summary>
        public int updateById(sys_user record)
        {
            return new SysUserDao().updateById(record);
        }

        /// <summary>
        /// getById
        /// </summary>
        public sys_user getById(int id)
        {
            return new SysUserDao().getById(id);
        }
    }
  }
