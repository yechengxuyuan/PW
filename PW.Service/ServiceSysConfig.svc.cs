using PW.DBCommon.Dao;
  using PW.DBCommon.Model;
  using System.Collections.Generic;

  namespace PW.Service
  {
    public class ServiceSysConfig : IServiceSysConfig
    {
        /// <summary>
        /// 查询
        /// </summary>
        public List<sys_config> query(sys_config record)
        {
            return new SysConfigDao().query(record);
        }
 
        /// <summary>
        /// 分页查询
        /// </summary>
        public PageInfo<sys_config> queryPage( PageInfo<sys_config> record)
        {
            return new SysConfigDao().queryPage(record);
        }

        /// <summary>
        /// 删除
        /// </summary>
        public int deleteById(int id)
        {
            return new SysConfigDao().deleteById(id);
        }

        /// <summary>
        /// 添加
        /// </summary>
        public int add(sys_config record)
        {
            return new SysConfigDao().add(record);
        }

        /// <summary>
        /// 更新
        /// </summary>
        public int updateById(sys_config record)
        {
            return new SysConfigDao().updateById(record);
        }

        /// <summary>
        /// getById
        /// </summary>
        public sys_config getById(int id)
        {
            return new SysConfigDao().getById(id);
        }
    }
  }